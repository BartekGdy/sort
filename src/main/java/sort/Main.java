package sort;

import sort.strategies.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        PorownywarkaAlgorytmow porownywarka = new PorownywarkaAlgorytmow();

        Scanner sc = new Scanner(System.in);

        String liniaZWejscia = null;
        while (sc.hasNextLine()) {
            liniaZWejscia = sc.nextLine();
            if (liniaZWejscia.startsWith("ustaw ")) {
                System.out.println("Ustawiam algorytm: ");
                String algorytm = liniaZWejscia.replace("ustaw ", "");
                if (algorytm.equalsIgnoreCase("bubble")) {
                    System.out.print(" bubble sort");
                    porownywarka.setStrategia(new BubbleSort());
                } else if (algorytm.equalsIgnoreCase("quick")) {
                    System.out.print(" quick sort");
                    porownywarka.setStrategia(new QuickSort());
                } else if (algorytm.equalsIgnoreCase("insertion")) {
                    System.out.print(" insertion sort");
                    porownywarka.setStrategia(new InsertionSort());
                } else if (algorytm.equalsIgnoreCase("merge")) {
                    System.out.print(" merge sort");
                    porownywarka.setStrategia(new MergeSort());
                } else if (algorytm.equalsIgnoreCase("bucket")) {
                    System.out.print(" bucket sort");
                    porownywarka.setStrategia(new BucketSort());
                } else {
                    System.out.print("nieznany algorytm");
                }
            } else if (liniaZWejscia.equalsIgnoreCase("sort")) {
                porownywarka.sortuj();
            } else if (liniaZWejscia.equalsIgnoreCase("print")) {
                porownywarka.print();
            } else if (liniaZWejscia.equalsIgnoreCase("measure")) {
                porownywarka.sortAndTime();
            } else if (liniaZWejscia.equalsIgnoreCase("quit")) {
                break;
            }
        }

    }
}
